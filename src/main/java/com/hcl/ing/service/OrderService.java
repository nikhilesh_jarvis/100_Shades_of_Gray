package com.hcl.ing.service;

import java.util.List;

import com.hcl.ing.model.Order;

public interface OrderService {	
	
     
    void saveOrder(Order order); 
    List<Order> findAllOrders();  

}
