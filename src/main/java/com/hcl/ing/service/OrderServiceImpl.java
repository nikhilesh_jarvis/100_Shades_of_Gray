package com.hcl.ing.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.ing.model.Order;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService{
	

	
	private static List<Order> orders;
	
	static{
        orders = populateDummyOrders();
    }

	@Override
	public void saveOrder(Order order) {		
		orders.add(order);
		
	}

	@Override
	public List<Order> findAllOrders() {	
		return orders;
	}
	
	private static List<Order> populateDummyOrders(){
        List<Order> orders = new ArrayList<Order>();        
       
        orders.add(new Order(11111,"US","India", 5000.00, "19-11-2017 10:27:44","US"));
        orders.add(new Order(22222,"UK","India", 6000.00, "19-11-2017 10:27:44","US"));
        orders.add(new Order(33333,"UAE","India", 7000.00, "19-11-2017 10:27:44","US"));
       return orders;
    }

}
