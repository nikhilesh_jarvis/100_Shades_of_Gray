package com.hcl.ing.model;

import java.util.Date;

public class Order {

	private long userId;
	private String from;
	private String to;
	private double toSell;
	private String timePlace;
	private String originCountry;
	
	
	
	public Order() {
		super();
	}

	public Order(long userId, String from, String to, double toSell,  String timePlace, String originCountry) {
		this.userId = userId;
		this.from = from;
		this.to = to;
		this.toSell = toSell;
		this.timePlace = timePlace;
		this.originCountry = originCountry;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public double getToSell() {
		return toSell;
	}

	public void setToSell(double toSell) {
		this.toSell = toSell;
	}

	public String getTimePlace() {
		return timePlace;
	}

	public void setTimePlace(String timePlace) {
		this.timePlace = timePlace;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	
}
